package web

// *merupakan struct yang digunakan untuk menampung data yang dikirimkan oleh client, saling berhubung dengnan category services
type CategoryCreateRequest struct {
	Name string `validate:"required,max=200,min=2" json:"name"`
}
