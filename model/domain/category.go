package domain

// *merupakan model entity di dalam domain model

type Category struct {
	Id   int
	Name string
}
