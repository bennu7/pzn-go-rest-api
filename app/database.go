package app

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/bennu7/pzn-go-rest-api.git/helper"
	"time"
)

func NewDB() *sql.DB {
	db, err := sql.Open("mysql", "root:Flypower167/\\@tcp(localhost:3306)/pzn_go_rest_api")
	helper.PanicIfError(err)

	db.SetMaxIdleConns(5)
	db.SetMaxOpenConns(20)
	db.SetConnMaxLifetime(60 * time.Minute)
	db.SetConnMaxIdleTime(10)

	return db
}
