package main

import (
	"github.com/go-playground/validator/v10"
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/bennu7/pzn-go-rest-api.git/app"
	"gitlab.com/bennu7/pzn-go-rest-api.git/controller"
	"gitlab.com/bennu7/pzn-go-rest-api.git/exception"
	"gitlab.com/bennu7/pzn-go-rest-api.git/helper"
	"gitlab.com/bennu7/pzn-go-rest-api.git/middleware"
	"gitlab.com/bennu7/pzn-go-rest-api.git/repository"
	"gitlab.com/bennu7/pzn-go-rest-api.git/services"
	"net/http"
)

func main() {
	validate := validator.New()
	db := app.NewDB()

	categoryRepository := repository.NewCategoryRepository()
	categoryService := services.NewCategoryService(categoryRepository, db, validate)
	categoryController := controller.NewCategoryController(categoryService)
	router := app.NewRouter(categoryController)

	// *dipindahkan router ke app.router.go
	//router := httprouter.New()
	//router.GET("/api/categories", categoryController.FindAll)
	//router.GET("/api/categories/:categoryId", categoryController.FindById)
	//router.POST("/api/categories", categoryController.Create)
	//router.PUT("/api/categories/:categoryId", categoryController.Update)
	//router.DELETE("/api/categories/:categoryId", categoryController.Delete)

	router.PanicHandler = exception.ErrorHandler

	server := http.Server{
		Addr: "localhost:8080",
		//Handler: router, *gunakan middleware sebagai routing
		Handler: middleware.NewAuthMiddleware(router),
	}

	err := server.ListenAndServe()
	helper.PanicIfError(err)
}
