package repository

import (
	"context"
	"database/sql"
	"gitlab.com/bennu7/pzn-go-rest-api.git/model/domain"
)

/*
 * PZN NOTE :
 ! TODO terlebih dahulu membuat kontrak seteleh membuat respoitory, gunakan kontrak interface
 * biasakan menggunakan context sebagai parameter pertama untuk kontrak interface
 ? parameter kedua untuk db transaksional wajib menggunkaan pointer, jika tidak akan menggunakan pointer akan berbahaya saat dikirim data sebagai bukan pointer nanti akan di copy datanya, pelajari lebih lanjut lagi untuk golang transactional
*/

type CategoryRepository interface {
	Save(ctx context.Context, tx *sql.Tx, category domain.Category) domain.Category
	Update(ctx context.Context, tx *sql.Tx, category domain.Category) domain.Category
	Delete(ctx context.Context, tx *sql.Tx, category domain.Category)
	FindById(ctx context.Context, tx *sql.Tx, categoryId int) (domain.Category, error)
	FindAll(ctx context.Context, tx *sql.Tx) []domain.Category
}
