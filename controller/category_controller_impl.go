package controller

import (
	"github.com/julienschmidt/httprouter"
	"gitlab.com/bennu7/pzn-go-rest-api.git/helper"
	"gitlab.com/bennu7/pzn-go-rest-api.git/model/web"
	"gitlab.com/bennu7/pzn-go-rest-api.git/services"
	"net/http"
	"strconv"
)

type CategoryControllerImpl struct {
	CategoryService services.CategoryService
}

// *function untuk memudahkan urusan routing ke main file, parameter service dan return nya controller
// *buat constructor setelah semua interface dan method jadi
func NewCategoryController(categoryService services.CategoryService) CategoryController {
	return &CategoryControllerImpl{
		CategoryService: categoryService,
	}

}

func (controller *CategoryControllerImpl) Create(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
	// *dipisahkan dan buat helper untuk request
	//decoder := json.NewDecoder(request.Body)
	//
	//categoryCreateRequest := web.CategoryCreateRequest{}
	//err := decoder.Decode(&categoryCreateRequest)
	//helper.PanicIfError(err)

	categoryCreateRequest := web.CategoryCreateRequest{}
	helper.ReadFromRequestBody(request, &categoryCreateRequest)

	categoryResponse := controller.CategoryService.CreateCategory(request.Context(), categoryCreateRequest)

	webResponse := web.WebResponse{
		Code:   http.StatusCreated,
		Status: "OK",
		Data:   categoryResponse,
	}

	// *dipisahkan dan buat helper untuk writer
	//writer.Header().Add("Content-Type", "application/json")
	//encoder := json.NewEncoder(writer)
	//err := encoder.Encode(&webResponse)
	//helper.PanicIfError(err)
	helper.WriteToResponseCreatedBody(writer, &webResponse)
}

func (controller *CategoryControllerImpl) Update(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
	//decoder := json.NewDecoder(request.Body)
	//
	//categoryUpdateRequest := web.CategoryUpdateRequest{}
	//err := decoder.Decode(&categoryUpdateRequest)
	//helper.PanicIfError(err)

	categoryUpdateRequest := web.CategoryUpdateRequest{}
	helper.ReadFromRequestBody(request, &categoryUpdateRequest)

	categoryId := params.ByName("categoryId")
	id, err := strconv.Atoi(categoryId)
	helper.PanicIfError(err)
	categoryUpdateRequest.Id = id

	categoryResponse := controller.CategoryService.UpdateCategory(request.Context(), categoryUpdateRequest)

	webResponse := web.WebResponse{
		Code:   http.StatusOK,
		Status: "OK",
		Data:   categoryResponse,
	}

	//writer.Header().Add("Content-Type", "application/json")
	//encoder := json.NewEncoder(writer)
	//err = encoder.Encode(&webResponse)
	//helper.PanicIfError(err)
	helper.WriteToResponseOkBody(writer, webResponse)
}

func (controller *CategoryControllerImpl) Delete(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {

	categoryId := params.ByName("categoryId")
	id, err := strconv.Atoi(categoryId)
	helper.PanicIfError(err)

	controller.CategoryService.DeleteCategory(request.Context(), id)

	webResponse := web.WebResponse{
		Code:   http.StatusOK,
		Status: "OK",
		Data:   "",
	}

	helper.WriteToResponseOkBody(writer, webResponse)
}

func (controller *CategoryControllerImpl) FindById(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
	categoryId := params.ByName("categoryId")
	id, err := strconv.Atoi(categoryId)
	helper.PanicIfError(err)

	categoryResponse := controller.CategoryService.FindByIdCategory(request.Context(), id)

	webResponse := web.WebResponse{
		Code:   http.StatusOK,
		Status: "OK",
		Data:   categoryResponse,
	}

	helper.WriteToResponseOkBody(writer, webResponse)
}

func (controller *CategoryControllerImpl) FindAll(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
	categoryResponses := controller.CategoryService.FindAllCategory(request.Context())

	webResponse := web.WebResponse{
		Code:   http.StatusOK,
		Status: "OK",
		Data:   categoryResponses,
	}

	helper.WriteToResponseOkBody(writer, webResponse)
}
